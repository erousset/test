extends "res://unittest.gd"

func tests():
    testcase("board can place pieces and check for collisions")
    assert_false(5==5, 'near top left corner')
    endcase()

    testcase("rotating a piece 4x results in the original piece for all pieces")
    for i in range(10):
        assert_eq(i, i+1-1, '4x rotation piece ' + str(i))
    endcase()